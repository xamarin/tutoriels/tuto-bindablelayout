# Application Xamarin - Tuto Bindable Layout

## Description

Ce repo git contient le code source de l'application developpé dans le cadre du tuto d'utilisation du Bindable Layout en Xamarin.
Il s'agit d'une petite application contenant une liste d'animaux à l'affichage personnalisé et cliquable.
Le tutoriel est consultable dans la section `Wiki`.

## Utilisation

Le code a été developpé sur Visual Studio 2019 et l'application est à destination d'un appareil Android. Le déploiement vers un appareil iOS n'a pas été testé.

## Contact

Pour signaler tout bug, erreur de typo ou autres remarques:

Martin Toutant

Email : `martin.toutant@inrae.fr`
