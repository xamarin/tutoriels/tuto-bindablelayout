﻿using System;

namespace BindableLayoutSample.Model
{
    // Le modèle Animal avec ses 3 propriétés et accesseurs
    public class Animal
    {
        public int AnimalID { get; set; }
        public string Elevage { get; set; }
        public DateTime DateTraitement { get; set; }

    }
}
