﻿using BindableLayoutSample.Model;
using BindableLayoutSample.View;
using BindableLayoutSample.ViewModel.Base;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace BindableLayoutSample.ViewModel
{
    public class AnimalItemViewModel : ViewModelBase
    {
        private Animal animal;

        // Les infos de l'animal
        public Animal Animal
        {
            get => animal;
            set => SetProperty(ref animal, value);
        }

        // La commande de navigation est asynchrone,
        // Pour ne pas bloquer le Thread principal en charge de l'affichage
        // pendant l'instanciation de la nouvelle Page
        private async Task NavigateToAnimalDetails()
        {
            // Petite attente pour rendre la transition plus fluide
            await Task.Delay(150);

            // L'utilisation de la navigation page nous permet d'avoir une navigation verticale (push & pop)
            await Application.Current.MainPage.Navigation.PushAsync(new AnimalDetailsView(Animal));
        }

        public ICommand NavigateToAnimalCommand { private set; get; }

        public AnimalItemViewModel(Animal _animal)
        {
            Animal = _animal;
            NavigateToAnimalCommand = new Command(async () => await NavigateToAnimalDetails());
        }

        public override string ToString()
        {
            return "" + Animal.AnimalID;
        }

    }
}
