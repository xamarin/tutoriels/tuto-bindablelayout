﻿using BindableLayoutSample.DAO;
using BindableLayoutSample.Model;
using BindableLayoutSample.ViewModel.Base;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace BindableLayoutSample.ViewModel
{
    // Le ViewModel de la page d'affichage de la liste d'animaux
    public class AnimalListViewModel : ViewModelBase
    {
        #region Attributes
        private bool isBusy;
        private bool isLoadedAnimals;
        #endregion

        #region Properties
        // Indicateur de travail en cours
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        // Indicateur sur l'état de chargement des données animaux
        public bool IsLoadedAnimals
        {
            get => isLoadedAnimals;
            set => SetProperty(ref isLoadedAnimals, value);
        }

        // La liste d'animaux qui sera alimentée avec les données récupérées par la DAO
        // L'objet ObservableCollection implémente directement INPC
        public ObservableCollection<AnimalItemViewModel> AnimalList { get; set; }
        #endregion

        #region Commands
        // La commande de chargement des données animaux
        // Asynchrone pour ne pas bloquer le thread principal
        // et donc pour ne pas bloquer l'affichage
        private async Task LoadAnimalCommand()
        {
            IsBusy = true;
            IsLoadedAnimals = false;
            AnimalList.Clear();

            AnimalDAO dao = new AnimalDAO();
            List<Animal> result = await dao.GetAllAnimals();
            
            // On alimente la liste observée par la view (AnimalList)
            foreach (Animal _animal in result)
            {
                AnimalItemViewModel toAddItem = new AnimalItemViewModel(_animal);
                AnimalList.Add(toAddItem);
            }

            IsLoadedAnimals = true;
            IsBusy = false;
        }

        public ICommand LoadAnimalsCommand { private set; get; }
        #endregion 
        
        // CTOR
        public AnimalListViewModel()
        {
            // Il important d'initialiser les listes d'un ViewModel pour éviter
            // les NullPointerExceptions
            AnimalList = new ObservableCollection<AnimalItemViewModel>();
            LoadAnimalsCommand = new Command(async () => await LoadAnimalCommand());
        }
    }
}
