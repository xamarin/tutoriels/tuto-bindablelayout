﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BindableLayoutSample.ViewModel.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        // Propriété de notification de changement
        public event PropertyChangedEventHandler PropertyChanged;

        // Procédure de changement de valeur
        // Déclenche la fonction de notification
        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyname = null)
        {
            if (Equals(storage, value))
            {
                return false;
            }

            storage = value;
            OnPropertyChanged(propertyname);
            return true;
        }

        // Fonction de notification 
        // Déclenche l'évènement de changement de valeur
        // Avec en argument le nom de la propriété qui a changé
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
