﻿using BindableLayoutSample.Model;
using BindableLayoutSample.ViewModel.Base;

namespace BindableLayoutSample.ViewModel
{
    // Le ViewModel de la page d'information sur chaque animal
    // Instancié avec un objet de type Animal passé en argument
    public class AnimalDetailsViewModel : ViewModelBase
    {
        private Animal animal;

        public Animal Animal
        {
            get => animal;
            set => SetProperty(ref animal, value);
        }

        public AnimalDetailsViewModel(Animal _animal)
        {
            Animal = _animal;
        }
    }
}
