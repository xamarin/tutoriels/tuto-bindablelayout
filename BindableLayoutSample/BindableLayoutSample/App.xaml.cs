﻿using BindableLayoutSample.View;
using Xamarin.Forms;

namespace BindableLayoutSample
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Point d'entrée de l'application
            MainPage = new NavigationPage(new AnimalListView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
