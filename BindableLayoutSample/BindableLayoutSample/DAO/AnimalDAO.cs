﻿using BindableLayoutSample.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BindableLayoutSample.DAO
{
    // La classe d'accès aux données sur les animaux
    public class AnimalDAO
    {
        public async Task<List<Animal>> GetAllAnimals()
        {
            // Génération de fausse données
            List<Animal> ret = new List<Animal>()
            {
                new Animal()
                {
                    AnimalID = 27852,
                    DateTraitement = DateTime.Parse("2022-08-05 14:26"),
                    Elevage = "PEIMA"
                },
                new Animal()
                {
                    AnimalID = 2421,
                    DateTraitement = DateTime.Parse("2022-08-05 12:33"),
                    Elevage = "PEIMA"
                },
                new Animal()
                {
                    AnimalID = 13569,
                    DateTraitement = DateTime.Parse("2022-08-06 08:14"),
                    Elevage = "Lees-Athas"
                },
                new Animal()
                {
                    AnimalID = 45555,
                    DateTraitement = DateTime.Parse("2022-08-07 16:21"),
                    Elevage = "Donzacq"
                },
                new Animal()
                {
                    AnimalID = 8956,
                    DateTraitement = DateTime.Parse("2022-08-07 18:01"),
                    Elevage = "PEIMA"
                },
            };

            // Simulation d'attente d'accès aux données (WS, BDD, ...)
            await Task.Delay(1000);
            return ret;
        }
    }
}
