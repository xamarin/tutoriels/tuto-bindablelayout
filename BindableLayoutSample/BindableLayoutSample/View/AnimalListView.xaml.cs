﻿using BindableLayoutSample.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BindableLayoutSample.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimalListView : ContentPage
    {
        public AnimalListView()
        {
            InitializeComponent();
            BindingContext = new AnimalListViewModel();
        }
    }
}