﻿using BindableLayoutSample.Model;
using BindableLayoutSample.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BindableLayoutSample.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimalDetailsView : ContentPage
    {
        public AnimalDetailsView(Animal animal)
        {
            InitializeComponent();
            BindingContext = new AnimalDetailsViewModel(animal);
        }
    }
}